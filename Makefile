CC := gcc
CFLAGS := -march=native

TARGET:= matrix

BUILD_DIR := ./build
SRC_DIR := ./src
INC_DIR := ./inc

SRCS := $(shell find $(SRC_DIRS) -name *.c)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
INC_FLAGS := $(addprefix -I,$(INC_DIR))

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $@

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC_FLAGS) -c $< -o $@

clean:
	rm $(TARGET)
	rm -r $(BUILD_DIR)