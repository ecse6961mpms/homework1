#ifndef __MATRIX_H__
#define __MATRIX_H__


#ifndef __intrin
#define __intrin
#include <immintrin.h>
#include <xmmintrin.h>
#endif

#include <stdint.h>

#define FIXED_Q 8


typedef struct mf32{
    float** data;
    int m;
	int n_int;
    int n;
    
} mf32; 


//packed version
typedef struct mf32p{
	__m256** data;
	mf32* src;
} mf32p; 


typedef struct mi16{
	int16_t** data;

	int m;
	int n;
} mi16; 

void mult_4x4(mf32* A, mf32* B, mf32* dest);

int mat_mult1(mf32* A, mf32* B, mf32* dest);

void mat_mult(mf32* A, mf32* B, mf32* dest);
int mat_mult_naive(mf32* A, mf32* B, mf32* dest);
void mat_mult_i16(mi16* A, mi16* B, mi16* dest);
int mat_mult_naive_i16(mi16* A, mi16* B, mi16* dest);

void transpose(mf32* M);
void transpose_i16(mi16* M);

int cmp_matricies(mf32* A, mf32* B, float e);
int cmp_matricies_i16(mi16* A, mi16* B);

mf32* mat_init(int m, int n);
mf32* zeros(int m, int n);
mf32* random_matrix(int m, int n);
mf32* dup_matrix(mf32* M);


mf32p* pack(mf32* M);
void unpack(mf32p* P);


mi16* zeros_i16(int m, int n);
mi16* random_matrix_i16(int m, int n);
mi16* dup_matrix_i16(mi16* M);


void delete_matrix(mf32* matrix);
void print_matrix(mf32* matrix);

void delete_matrix_i16(mi16* matrix);
void print_matrix_i16(mi16* matrix);

#endif
