#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "matrix.h"

float test_e = 0.00001; //floats, amirite?

void transposeTest(mf32* M){
	printf("Starting Matrix Transposition Test\n------------------------------------------------------\n");
	printf("Intial Matrix:\n--------------------------------------\n");
	print_matrix(M);
	mf32* M_temp = dup_matrix(M);
	transpose(M_temp);
	printf("Transposed Matrix:\n--------------------------------------\n");
	print_matrix(M_temp);
	transpose(M_temp);
	printf("Re-Transposed Matrix:\n--------------------------------------\n");
	print_matrix(M_temp);
	if(cmp_matricies(M, M_temp, test_e)){
		printf("Matricies are equivalent, with e=%.2e\n", test_e);
	} else {
		printf("Matricies are mismatched, with e=%.2e\n", test_e);
	}
	printf("END OF TEST\n------------------------------------------------------\n\n\n");
	delete_matrix(M_temp);
}

void mul_4x4_test(mf32* A, mf32* B){
	printf("Starting 4x4 Matrix Multiplication Test\n------------------------------------------------------\n");
	printf("Intial Matricies:\n--------------------------------------\n");
	printf("A:\n");
	print_matrix(A);
	printf("B:\n");
	print_matrix(B);

	mf32* res1 = zeros(A->m, B->n);
	mf32* res2 = zeros(A->m, B->n);
	
	printf("Result Matrix (Naive):\n--------------------------------------\n");
	mat_mult_naive(A, B, res1);
	print_matrix(res1);
	
	printf("Result Matrix (SIMD):\n--------------------------------------\n");
	mult_4x4(A, B, res2);
	print_matrix(res2);
	if(cmp_matricies(res1, res2, test_e)){
		printf("Matricies are equivalent, with e=%.2e\n", test_e);
	} else {
		printf("Matricies are mismatched, with e=%.2e\n", test_e);
	}
	printf("END OF TEST\n------------------------------------------------------\n\n\n");
	delete_matrix(res1);
	delete_matrix(res2);
}


int mul1_test(int m, int n, int o ){
	//Do not use!! segfaults at the pack() function
	clock_t before, after;
	clock_t t1, t2;
	printf("Starting Matrix Mult1 Test\n------------------------------------------------------\n");
	printf("Intial Matricies:\n--------------------------------------\n");
	mf32* A = random_matrix(m,n);
	if(A == NULL){
		printf("A alloc failed\n");
		return -1;
	}

	mf32* B = random_matrix(n,o);
	if(B==NULL){
		printf("B alloc failed\n");
		return -1;
	}
	
	printf("A:\n");
	print_matrix(A);
	printf("B:\n");
	print_matrix(B);

	mf32* res1 = zeros(A->m, B->n);
	mf32* res2 = zeros(A->m, B->n);

	if(res1 == NULL || res2 == NULL){
		printf("Result matricies allocation failed\n");
		return -1;
	}
	
	printf("Result Matrix (Naive):\n--------------------------------------\n");
	before = clock();
	mat_mult_naive(A, B, res1);
	after = clock();
	t1 = after - before;
	print_matrix(res1);
	printf("Took %f s\n", (float)t1/CLOCKS_PER_SEC);
	
	printf("Result Matrix (SIMD):\n--------------------------------------\n");
	before = clock();
	mat_mult1(A, B, res2);
	after = clock();
	t2 = after - before;
	print_matrix(res2);
	printf("Took %f s\n", (float)t2/CLOCKS_PER_SEC);
	printf("%.2f %% faster with SIMD\n", ((float)t1-t2)/t2*100);
	if(cmp_matricies(res1, res2, test_e)){
		printf("Matricies are equivalent, with e=%.2e\n", test_e);
	} else {
		printf("Matricies are mismatched, with e=%.2e\n", test_e);
	}
	printf("END OF TEST\n------------------------------------------------------\n\n\n");
	delete_matrix(res1);
	delete_matrix(res2);
	return 0;
}

int mul_test(mf32* A, mf32* B){
	printf("Starting Matrix Multiplication Test\n------------------------------------------------------\n");
	printf("Intial Matricies:\n--------------------------------------\n");

	printf("A:\n");
	print_matrix(A);
	printf("B:\n");
	print_matrix(B);

	mf32* res1 = zeros(A->m, B->n);
	mf32* res2 = zeros(A->m, B->n);

	if(res1 == NULL || res2 == NULL) return -1;

	printf("Result Matrix (Naive):\n--------------------------------------\n");
	mat_mult_naive(A, B, res1);
	print_matrix(res1);
	
	printf("Result Matrix (SIMD):\n--------------------------------------\n");
	mat_mult(A, B, res2);
	print_matrix(res2);
	if(cmp_matricies(res1, res2, test_e)){
		printf("Matricies are equivalent, with e=%.2e\n", test_e);
	} else {
		printf("Matricies are mismatched, with e=%.2e\n", test_e);
	}
	printf("END OF TEST\n------------------------------------------------------\n\n\n");
	delete_matrix(res1);
	delete_matrix(res2);
	return 0;
}

int mul_test_i16(int n){
	mi16* A = random_matrix_i16(n,n);
    if(A == NULL){
        printf("A ALLOC FAILED\n");
        return -1;
    }
    mi16* B = random_matrix_i16(n,n);
    if(B == NULL){
        printf("B ALLOC FAILED\n");
        return -1;
    }
    mi16* d1 = zeros_i16(n,n);
    if(d1 == NULL){
        printf("d1 ALLOC FAILED\n");
        return -1;
    }
	mi16* d2 = zeros_i16(n,n);
	if(d2 == NULL){
		printf("d2 ALLOC FAILED\n");
		return -1;
	}


	printf("Starting Matrix Multiplication Test\n------------------------------------------------------\n");
	printf("Intial Matricies:\n--------------------------------------\n");
	printf("A:\n");
	print_matrix_i16(A);
	printf("B:\n");
	print_matrix_i16(B);
	printf("Result Matrix (Naive):\n--------------------------------------\n");
	mat_mult_naive_i16(A, B, d1);
    print_matrix_i16(d1);
    printf("Result Matrix (SIMD):\n--------------------------------------\n");
	mat_mult_i16(A, B, d2);
	print_matrix_i16(d2);
	if(cmp_matricies_i16(d1,d2)){
		printf("Matricies match\n");
	} else {
		printf("Matricies DO NOT MATCH\n");
	}
		
	delete_matrix_i16(A);
	delete_matrix_i16(B);
	delete_matrix_i16(d1);
	delete_matrix_i16(d2);
    return 0;

}


float test_float_file(FILE* fp, int numruns, int size){
	clock_t before, after, tacc_n, tacc_s;
	tacc_n = 0;
	tacc_s = 0;
	for(int i = 0; i < numruns; i++){
		mf32* A = random_matrix(size,size);
		mf32* B = random_matrix(size,size);
		mf32* d1= random_matrix(size,size);
		mf32* d2= random_matrix(size,size);
		if(A == NULL || B==NULL || d1 == NULL || d2==NULL)return (float)-1;
		before = clock();
		mat_mult_naive(A,B,d2);
		after = clock();
		tacc_n += (after - before);
		before = clock();
		mat_mult(A, B, d1);
		after = clock();
		tacc_s += (after-before);
	}
	float navg = (float)tacc_n/(numruns * CLOCKS_PER_SEC);
	float savg = (float)tacc_s/(numruns * CLOCKS_PER_SEC);
	float retval = (((float)tacc_n - tacc_s) / tacc_s) * 100; 
	if(1) printf("%f %f %f\n", navg, savg, retval);
	fprintf(fp, "%dx%d &  %.2f & %.2f & %.2f \\\\ \n", size,size, navg, savg, retval);
	return retval;
}

float test_fixed_file(FILE* fp, int numruns, int size){
	clock_t before, after, tacc_n, tacc_s;
	tacc_n = 0;
	tacc_s = 0;
	for(int i = 0; i < numruns; i++){
		mi16* A = random_matrix_i16(size,size);
		mi16* B = random_matrix_i16(size,size);
		mi16* d1= random_matrix_i16(size,size);
		mi16* d2= random_matrix_i16(size,size);
		if(A == NULL || B==NULL || d1 == NULL || d2==NULL)return (float)-1;
		before = clock();
		mat_mult_naive_i16(A,B,d2);
		after = clock();
		tacc_n += (after - before);
		before = clock();
		mat_mult_i16(A, B, d1);
		after = clock();
		tacc_s += (after-before);
	}
	float navg = (float)tacc_n/(numruns * CLOCKS_PER_SEC);
	float savg = (float)tacc_s/(numruns * CLOCKS_PER_SEC);
	float retval = (((float)tacc_n - tacc_s) / tacc_s) * 100; 
	if(1) printf("%f %f %f\n", navg, savg, retval);
	fprintf(fp, "%dx%d &  %.2f & %.2f & %.2f \\\\ \n", size,size, navg, savg, retval);
	return retval;
}

void test_to_file(FILE* fp, int test, int numruns, int numsizes, int* sizes){
	fprintf(fp, "\\begin{tabular}{l | l | l | l} \n");
	if(test & 0x01){
		fprintf(fp, "Floating point test & & & \\\\ Mat. Size & Naive (s) & SIMD (s) & \\%% speedup \\\\ \\hline");
		float average = 0;
		for(int i=0; i< numsizes; i++){ 
			average += test_float_file(fp, numruns, sizes[i]);
			if(1) printf("%f \n", average);
		}
		average = average / (float)numsizes;
		fprintf(fp, "\\hline\nAVG & & & %.2f \\\\ \n", average);
	}
	fprintf(fp, "\n\n\n");
	if(test & 0x02){
		fprintf(fp, "Fixed point test & & & \\\\ Mat. Size & Naive (s) & SIMD (s) & \\%% speedup \\\\ \\hline"); 
		float average = 0;
		for(int i=0; i<numsizes; i++) average += test_fixed_file(fp, numruns, sizes[i]);
		average /= (float)numsizes;
		fprintf(fp, "\\hline\nAVG & & & %.2f \\\\ \n",average);
	}
	fprintf(fp, "\\end{tabular}\n");

}

int time_mult(int n){
    clock_t before, after;
    clock_t t1, t2;
    printf("Allocating 3 %dx%d matrices\n", n, n);
    mf32* A = random_matrix(n,n);
    if(A == NULL){
        printf("A ALLOC FAILED\n");
        return -1;
    }
    mf32* B = random_matrix(n,n);
    if(B == NULL){
        printf("B ALLOC FAILED\n");
        return -1;
    }
    mf32* d = zeros(n,n);
    if(d == NULL){
        printf("d ALLOC FAILED\n");
        return -1;
    }

    printf("Preforming unoptimized multiplication...\n");
    before = clock();
    mat_mult_naive(A, B, d);
    after = clock();
    t1 = after - before;
    printf("took %.4f s\n", (float)t1/CLOCKS_PER_SEC);

    printf("Preforming optimized multiplication 1...\n");
    before = clock();
    mat_mult(A, B, d);
    after = clock();
    t2 = after - before;
    printf("took %.4f s\n", (float)t2/CLOCKS_PER_SEC);

    printf("Speedup of %.2f%% using SIMD instructions\n", ((float)t1-t2)/t2*100);

    delete_matrix(A);
    delete_matrix(B);
    delete_matrix(d);
    return 0;
}

int time_mul_i16(int n){
    clock_t before, after;
    clock_t t1, t2;
	printf("Allocating 3 %dx%d matrices\n", n, n);
	mi16* A = random_matrix_i16(n,n);
    if(A == NULL){
        printf("A ALLOC FAILED\n");
        return -1;
    }
    mi16* B = random_matrix_i16(n,n);
    if(B == NULL){
        printf("B ALLOC FAILED\n");
        return -1;
    }
    mi16* d1 = zeros_i16(n,n);
    if(d1 == NULL){
        printf("d1 ALLOC FAILED\n");
        return -1;
    }

	printf("Preforming unoptimized multiplication...\n");
    before = clock();
	mat_mult_naive_i16(A, B, d1);
	after = clock();
	t1 = after - before;
	printf("took %.4f s\n", (float)t1/CLOCKS_PER_SEC);
    printf("Preforming optimized multiplication 1...\n");
	before = clock();
	mat_mult_i16(A, B, d1);
	after = clock();
	t2 = after - before;
	printf("took %.4f s\n", (float)t2/CLOCKS_PER_SEC);
	printf("Speedup of %.2f%% using SIMD instructions\n", ((float)t1-t2)/t2*100);

	delete_matrix_i16(A);
	delete_matrix_i16(B);
	delete_matrix_i16(d1);
    return 0;

}

int main(int argc, char** argv){
	if(argc > 2 && argc <= 4){ 
		fprintf(stderr, "Usage: matrix [ n | [[FILE] [1|2|3] [NR] [Sizes...]] ]\n n = size of matrix to test for non-writeout test \nFILE = filename for output\n 1: floating point 2: fixed point 3: both\n NR = Number of Runs per size to be averaged\n Sizes = space separated list of matrix sizes to be run \n");
		return -1;
	} else if(argc == 1){
	    int n = 4;
		//if(DEBUG) printf("Allocating matricies\n");
	    mf32* A = random_matrix(n,n);
	   	if(A == NULL){
	        printf("A ALLOC FAILED\n");
	        return -1;
   		}
	    mf32* B = random_matrix(n,n);
	    if(B == NULL){
	        printf("B ALLOC FAILED\n");
	       return -1;
	   }
	   mf32* d = zeros(n,n);
	   if(d == NULL){
	       printf("d ALLOC FAILED\n");
	       return -1;
	   }
	
	   printf("matrix A:\n");
	   print_matrix(A);
	   printf("matrix B:\n");
	   print_matrix(B);

		mult_4x4(A, B, d);
	
		printf("A*B=\n");
		print_matrix(d);
		transposeTest(A);
		mul_4x4_test(A, B);
		mul_test(A, B);

		mf32* mat = mat_init(3, 17);
		printf("%d x %d matrix initialized, with %d x %d reserved in memory\n", mat->m, mat->n, mat->n_int, mat->n);

		//mul_test(100,100,100);
		//mul_test(1000,1000,1000);
		//mul_test(15, 23, 54);
	    delete_matrix(A);
    	delete_matrix(B);
    	delete_matrix(d);

    	n=20;
    	A = random_matrix(n,n);
    	if(A == NULL){
        	printf("A ALLOC FAILED\n");
        	return -1;
    	}
    	B = random_matrix(n,n);
    	if(B == NULL){
        	printf("B ALLOC FAILED\n");
        	return -1;
    	}
    	d = zeros(n,n);
    	if(d == NULL){
        	printf("d ALLOC FAILED\n");
        	return -1;
    	}
    	mul_test(A, B);

    	delete_matrix(A);
    	delete_matrix(B);
    	delete_matrix(d);

    	time_mult(100);
    	printf("\n");
    	//time_mult(1000);
    	printf("\n");
    	//time_mult(10000);
    	//printf("\n");

    	time_mul_i16(4);
		time_mul_i16(16);
		time_mul_i16(100);
		printf("Running basic tests with mi16:\n");
		mul_test_i16(100);

		printf("basic tests complete, please specify matrix size for benchmarking\n");
		
	} else if(argc == 2) {
		int n = atoi(argv[1]);
		printf("Testing mf32 matrix with dims %dx%d\n", n, n);
		time_mult(n);
		printf("\n");
		printf("Testing mi16 matrix with dims %dx%d\n", n, n);
		time_mul_i16(n);

	} else {
		//command line arguments given
		FILE * fp; 
		fp = fopen(argv[1] , "w+");
		if(fp == NULL) return -1;
		int tests = atoi(argv[2]);
		if(tests > 3 || tests < 0){
			fclose(fp);
			return -1;
		}
		int numruns = atoi(argv[3]);
		int sizes[argc-4];
		for(int i=0; i < argc-4; i++){
			sizes[i] = atoi(argv[i+4]);
		}
		test_to_file(fp, tests, numruns, argc-4, sizes);
		fclose(fp);
		return 0;
	}

}
