#include "matrix.h"

#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>

#ifndef __intrin
#define __intrin
#include <immintrin.h>
#include <xmmintrin.h>
#endif

#ifndef ERROR
#define ERROR 1
#endif

#ifndef DEBUG
#define DEBUG 1
#endif

//Debug prints for location
#ifndef DEBUG_LOC
#define DEBUG_LOC 0
#endif

// *******************************************************
// ******************** ROW MAJOR ************************
// *******************************************************

void mult_4x4(mf32* A, mf32* B, mf32* dest){		
    __m128 a1 = _mm_load_ps(A->data[0]);
    __m128 a2 = _mm_load_ps(A->data[1]);
    __m128 a3 = _mm_load_ps(A->data[2]);
    __m128 a4 = _mm_load_ps(A->data[3]);

    __m128 b1 = _mm_load_ps(B->data[0]);
    __m128 b2 = _mm_load_ps(B->data[1]);
    __m128 b3 = _mm_load_ps(B->data[2]);
    __m128 b4 = _mm_load_ps(B->data[3]);
	
    _MM_TRANSPOSE4_PS(b1, b2, b3, b4);

    __m128 result = _mm_set1_ps(0);
    result = _mm_add_ps(result, _mm_dp_ps(a1, b1, 0xF1));
    result = _mm_add_ps(result, _mm_dp_ps(a1, b2, 0xF2));
    result = _mm_add_ps(result, _mm_dp_ps(a1, b3, 0xF4));
    result = _mm_add_ps(result, _mm_dp_ps(a1, b4, 0xF8));
    _mm_store_ps(dest->data[0], result);

    result = _mm_set1_ps(0);
    result = _mm_add_ps(result, _mm_dp_ps(a2, b1, 0xF1));
    result = _mm_add_ps(result, _mm_dp_ps(a2, b2, 0xF2));
    result = _mm_add_ps(result, _mm_dp_ps(a2, b3, 0xF4));
    result = _mm_add_ps(result, _mm_dp_ps(a2, b4, 0xF8));
    _mm_store_ps(dest->data[1], result);
    result = _mm_set1_ps(0);
    result = _mm_add_ps(result, _mm_dp_ps(a3, b1, 0xF1));
    result = _mm_add_ps(result, _mm_dp_ps(a3, b2, 0xF2));
    result = _mm_add_ps(result, _mm_dp_ps(a3, b3, 0xF4));
    result = _mm_add_ps(result, _mm_dp_ps(a3, b4, 0xF8));
    _mm_store_ps(dest->data[2], result);
    result = _mm_set1_ps(0);
    result = _mm_add_ps(result, _mm_dp_ps(a4, b1, 0xF1));
    result = _mm_add_ps(result, _mm_dp_ps(a4, b2, 0xF2));
    result = _mm_add_ps(result, _mm_dp_ps(a4, b3, 0xF4));
    result = _mm_add_ps(result, _mm_dp_ps(a4, b4, 0xF8));
    _mm_store_ps(dest->data[3], result);
}

//For mat_mult1
mf32p* pack(mf32* M){
	mf32p* packed = malloc(sizeof(mf32p));
	if(packed == NULL){ 
		if(DEBUG) printf("FAILED to allocate packed struct\n");
		return NULL;
	} else { 
		if(DEBUG) printf("Allocated struct\n");
	}
	packed->src = M;
	packed->data = malloc(packed->src->m * sizeof(__m256*));
	if(packed->data == NULL){
		if(DEBUG) printf("FAILED TO ALLOCATE ROW POINTERS\n");
		free(packed);
		return NULL;
	} else { 
		if(DEBUG) printf("Allocated row pointers\n");
	}
	for(int i = 0; i < packed->src->m; i++){
		packed->data[i] = _mm_malloc((packed->src->n >> 3) * sizeof(__m256), 8);
		if(packed->data[i] == NULL){
			if(DEBUG) printf("FAILED TO ALLOCATE ROW %d \n", i);
			for(int rm = 0; rm < i; rm++) free(packed->data[rm]);
			free(packed->data);
			if(DEBUG) printf("Failed to allocate memory for row %d of %d", i, packed->src->m);
			free(packed);
			return NULL;
		}
		if(DEBUG) printf("Populating pack struct data for row %d\n", i);
		__m256 temp = _mm256_setzero_ps();
				for(int j=0; j < packed->src->n >>3; j++){
			if(DEBUG) printf("Populating data[%d][%d-%d]\n",i,j, j+7 );
			if(DEBUG){
				printf("%p, %p \n", packed->data[i], &packed->data[i][j+1]);
				packed->data[i][j] = temp; 
				printf("Successfully wrote to packed data\n");
			}
			packed->data[i][j] = _mm256_load_ps(packed->src->data[i] + j*8*sizeof(float));
		}
	}
	return packed;
}

void unpack(mf32p* P){
	for(int i = 0; i < P->src->m;i++){
		_mm_free(P->data[i]);
	}
	free(P->data);
	free(P);
}

int mat_mult1(mf32* A, mf32* B, mf32* dest){
	if(DEBUG) printf("IN MAT_MULT1\n");		
	if(dest->m != A->m || dest->n != B->n || A->n != B->m ) return -1;

	//mf32* Bt = dup_matrix(B);
	//transpose(Bt);
	mf32p* Bp = pack(B);
	if(DEBUG) printf("Success! packed B\n");
	__m256 result, A_curr;
	for(int i=0; i<dest->m; i++){
		for(int j=0; j<dest->n; j+=8){
			//iterate over destination
			//if(DEBUG) printf("i : %d | j : %d | k : %d\n ", i, j);
			result=_mm256_set1_ps(0.0);
			for(int k = 0; k < Bp->src->m; k++){
				if(DEBUG) printf("i : %d | j : %d | k : %d\n ", i, j, k);
				A_curr = _mm256_set1_ps(A->data[i][k]);
				result = _mm256_fmadd_ps(A_curr, Bp->data[k][j], result);
			}
			_mm256_store_ps((dest->data[i] + 8*j), result);
		}
	}
	unpack(Bp);
	return 0; // all good
}

int mat_mult_naive(mf32* A, mf32* B, mf32* dest){
	//if(A->n != B->m) throw std::runtime_error("Mismatched matrix sizes");
	//if(dest->m != A->m && dest->n != B->n) throw std::runtime_error("Destination matrix incorrectly formatted");
	if(dest->m != A->m || dest->n != B->n || A->n != B->m ) return -1;
	for(int i=0; i < A->m; i++){
		for(int j=0; j < A->n; j++){
			dest->data[i][j] = 0.0;
			for(int k=0; k < A->n; k++){
				dest->data[i][j] += A->data[i][k]*B->data[k][j];
			}
		}
	}
	return 0;
}

int mat_mult_naive_i16(mi16* A, mi16* B, mi16* dest){
	//if(A->n != B->m) throw std::runtime_error("Mismatched matrix sizes");
	//if(dest->m != A->m && dest->n != B->n) throw std::runtime_error("Destination matrix incorrectly formatted");
	if(dest->m != A->m || dest->n != B->n || A->n != B->m ) return -1;
	for(int i=0; i < A->m; i++){
		for(int j=0; j < A->n; j++){
			uint32_t temp = 0.0;
			for(int k=0; k < A->n; k++){
				temp += ((int32_t)A->data[i][k]*(int32_t)B->data[k][j]);
			}
			dest->data[i][j] = temp >> FIXED_Q;
		}
	}
	return 0;
}

void transpose(mf32* M){
	/* 
	 * Return to this if we want to cover non-square matricies. 
	 *
	if(M->m != M->n){
		mf32* temp = zeros(m->n, m->m);
	} else {
		temp = M;
	}*/
	#define swapf(x,y) {temp=x; x=y; y=temp;}
	float temp = 0.0;
	//start at 1 as diagonal doesn't need to be swapped
	for(int i=0; i < M->m; i++){
		for(int j=0; j<i; j++){ 
			//Go down until the diagonal is reached
			swapf(M->data[i][j], M->data[j][i]);
		}
	}
}

void transpose_i16(mi16* M){
	#define swapf(x,y) {temp=x; x=y; y=temp;}
	int16_t temp = 0;
	//start at 1 as diagonal doesn't need to be swapped
	for(int i=0; i < M->m; i++){
		for(int j=0; j<i; j++){ 
			//Go down until the diagonal is reached
			swapf(M->data[i][j], M->data[j][i]);
		}
	}
}

int cmp_matricies(mf32* A, mf32* B, float e){
	// Returns 0 if the matricies are mismatched, 1 if matricies are the same
	// return 0 if the sizes are mismatched
	if(A->m != B->m || A->n != B->n) return 0;
	// Check each element for equality, returning 0 at first mismatch.
	for(int i = 0; i < A->m; i++){
		for(int j=0; j < A->n; j++){
			//use an epsilon for float comparisons
            if(abs(A->data[i][j] - B->data[i][j])>e){
                printf("%f != %f\n", A->data[i][j], B->data[i][j]);
                return 0;
            }
        }
	}
	//If no element is mismatched, for loop ends, returning 1
	return 1;
}

int cmp_matricies_i16(mi16* A, mi16* B){
	if(A->m != B->m || A->n != B->n) return 0;
	for(int i = 0; i < A->m; i++){
		for(int j = 0; j < A->n; j++){
			if(A->data[i][j] != B->data[i][j]){
				printf("%d != %d at location [%d][%d]\n",A->data[i][j], B->data[i][j], i, j);
				return 0;
			}
		}
	}
	return 1;
}

void mat_mult(mf32* A, mf32* B, mf32* dest){
    transpose(B);
	for(int i=0; i < A->m; i++){
		for(int j=0; j < A->n; j++){
#ifdef __AVX__
            #define MULT_SIMD 8
            __m256 res = _mm256_setzero_ps();
#else
            #define MULT_SIMD 4
            __m128 res = _mm_setzero_ps();
#endif
			for(int k=0; k < A->n; k+=MULT_SIMD){
                //printf("(%d)\n",k);
                if(k+MULT_SIMD > A->n) for(int l = k; l < A->n; l++) res[0] += A->data[i][l]*B->data[j][l];
                else {
#ifdef __AVX__
				    __m256 a = _mm256_loadu_ps(&(A->data[i][k]));
                    __m256 b = _mm256_loadu_ps(&(B->data[j][k]));
                    //printf("(%d)=== %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f dot %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n", k, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7]);
                    res = _mm256_add_ps(res, _mm256_dp_ps(a, b, 0xF1));
#else
				    __m128 a = _mm_load_ps(&(A->data[i][k]));
                    __m128 b = _mm_load_ps(&(B->data[j][k]));
                    //printf("(%d)=== %.2f %.2f %.2f %.2f dot %.2f %.2f %.2f %.2f\n", k, a[0], a[1], a[2], a[3], b[0], b[1], b[2], b[3]);
                    res = _mm_add_ps(res, _mm_dp_ps(a, b, 0xF1));
#endif
                }
			}
#ifdef __AVX__
            dest->data[i][j] = res[0] + res[4];
#else
            dest->data[i][j] = res[0];
#endif
		}
	}
    transpose(B);
}

void mat_mult_i16(mi16* A, mi16* B, mi16* dest){
    transpose_i16(B);
	for(int i=0; i < A->m; i++){
		for(int j=0; j < A->n; j++){
	#ifdef I_AVX_TEST
            #define MULT_SIMD_I 16
            __m256i res = _mm256_setzero_si256();
	#else
            #define MULT_SIMD_I 8
            __m128i res = _mm_setzero_si128();
	#endif
			for(int k=0; k < A->n; k+=MULT_SIMD_I){
                if(k+MULT_SIMD_I > A->n) for(int l = k; l < A->n; l++) res[0] += A->data[i][l]*B->data[j][l];
                else {
			#ifdef I_AVX_TEST
				    __m256i a = _mm256_loadu_si256((__m256i*)&(A->data[i][k]));
                    __m256i b = _mm256_loadu_si256((__m256i*)&(B->data[j][k]));
                    res = _mm256_add_epi16(res, _mm256_madd_epi16(a, b));

			#else
				    __m128i a = _mm_loadu_si128((__m128i*)&(A->data[i][k]));
                    __m128i b = _mm_loadu_si128((__m128i*)&(B->data[j][k]));
                    //printf("=== %lld %lld %lld %lld %lld %lld %lld %lld \n", a[0]&0xFFFF, (a[0]>>16)&0xFFFF, (a[0]>>32)&0xFFFF, (a[0]>>48)&0xFFFF, a[1]&0xFFFF, (a[1]>>16)&0xFFFF, (a[1]>>32)&0xFFFF, (a[1]>>48)&0xFFFF);
                    res = _mm_add_epi32(res, _mm_madd_epi16(a, b));
                    //printf("=== %lld %lld %lld %lld %lld %lld %lld %lld \n", res[0]&0xFFFF, (res[0]>>16)&0xFFFF, (res[0]>>32)&0xFFFF, (res[0]>>48)&0xFFFF, res[1]&0xFFFF, (res[1]>>16)&0xFFFF, (res[1]>>32)&0xFFFF, (res[1]>>48)&0xFFFF);
			#endif
                }
			}
		#ifdef I_AVX_TEST
            res = _mm256_hadd_epi32(res, _mm256_setzero_si256());
            res = _mm256_hadd_epi32(res, _mm256_setzero_si256());
            res = _mm256_hadd_epi32(res, _mm256_setzero_si256());
            res = _mm256_hadd_epi32(res, _mm256_setzero_si256());
            dest->data[i][j] = res[0] >> FIXED_Q;
		#else
            res = _mm_hadd_epi32(res, _mm_setzero_si128());
            res = _mm_hadd_epi32(res, _mm_setzero_si128());
            dest->data[i][j] = res[0] >> FIXED_Q;
		#endif
		}
	}
    transpose_i16(B);
}

// *******************************************************
// ******************* INITIALIZERS **********************
// *******************************************************

mf32* mat_init(int m, int n){
	/*
	 * Returns an empty matrix with allocated memory
	 */
	mf32* matrix = malloc(sizeof(mf32));
	if(matrix == NULL){
		printf("FAILED TO ALLOCATE MATRIX STRUCT\n");
		return NULL;
	}
    matrix->m = m;
    matrix->n = n;
	int n_int = ((n >> 3) + 1) << 3;
	matrix->n_int = n_int;
    matrix->data = malloc(m*sizeof(float*));
    if(matrix->data == NULL){
		if(DEBUG) printf("FAILED TO ALLOCATE MATRIX ROWS\n");
        free(matrix);
        return NULL;
    } 
    for(int i = 0; i < m; i++){
        matrix->data[i] = aligned_alloc(sizeof(float), n_int*sizeof(float));//calloc(n, sizeof(float));
        if(matrix->data[i] == NULL){
			if(DEBUG) printf("FAILED TO ALLOCATE ROW: %d\n", i);
            for(int k = 0; k < i; k++) free(matrix->data[k]);
            free(matrix->data);
            free(matrix);
            return NULL;
        }
        for(int j = 0; j < n; j++){
            matrix->data[i][j] = 0.0;
        }
    }
	if(DEBUG_LOC) printf("Successfully initialized matrix\n");
	return matrix;
}


mf32* zeros(int m, int n){
	mf32* matrix = mat_init(m, n);
	if(matrix == NULL) return NULL;
	for(int i=0; i<matrix->m; i++){
		for(int j=0; j<matrix->n_int; j++){
			matrix->data[i][j] = 0.0;
		}
	}	
    return matrix;
}

mf32* random_matrix(int m, int n){
    mf32* matrix = zeros(m, n);
	if(matrix == NULL){
		if(DEBUG) printf("Failed to initialize random matrix\n");
		return NULL;
	}
	for(int i = 0; i < m; i++){	
        if(DEBUG_LOC) printf("row %d :\nElement:",i);
		for(int j = 0; j < n; j++){
			if(DEBUG_LOC)printf("%d ", j);
            matrix->data[i][j] = (rand()%1000)/1000.0;
        }
	}
	if(DEBUG_LOC) printf("Successfully created %d x %d random matrix", m,n);
	return matrix;
}

mf32* dup_matrix(mf32* M){
	mf32* dup = zeros(M->m, M->n);
	if(dup == NULL) return NULL;
	for(int i = 0; i < dup->m; i++){
		for(int j = 0; j < dup->n; j++){
			dup->data[i][j] = M->data[i][j];
		}
	}
	return dup;
}

// mi16* versions ----------------------------------------

mi16* zeros_i16(int m, int n){
    mi16* matrix = malloc(sizeof(mi16));
    if(matrix == NULL) return NULL;
    matrix->m = m;
    matrix->n = n;
    matrix->data = malloc(m*sizeof(int16_t*));
    if(matrix->data == NULL){
        free(matrix);
        return NULL;
    } 
    for(int i = 0; i < m; i++){
        matrix->data[i] = aligned_alloc(sizeof(int16_t), n*sizeof(int16_t));//calloc(n, sizeof(float));
        if(matrix->data[i] == NULL){
            for(int k = 0; k < i; k++) free(matrix->data[k]);
            free(matrix->data);
            free(matrix);
            return NULL;
        }
        for(int j = 0; j < n; j++){
            matrix->data[i][j] = 0.0;
        }
    }

    return matrix;
}

mi16* random_matrix_i16(int m, int n){
    mi16* matrix = malloc(sizeof(mi16));
    if(matrix == NULL) return NULL;
    matrix->m = m;
    matrix->n = n;
    matrix->data = malloc(m*sizeof(int16_t*));
    if(matrix->data == NULL){
        free(matrix);
        return NULL;
    } 
    for(int i = 0; i < m; i++){
        matrix->data[i] = aligned_alloc(sizeof(int16_t), n*sizeof(int16_t));//malloc(n*sizeof(float));
        if(matrix->data[i] == NULL){
            for(int k = 0; k < i; k++) free(matrix->data[k]);
            free(matrix->data);
            free(matrix);
            return NULL;
        }
        for(int j = 0; j < n; j++){
            matrix->data[i][j] = ((rand()%1000)/1000.0)*(1<<FIXED_Q);
        }
    }

    return matrix;
}

mi16* dup_matrix_i16(mi16* M){
	mi16* new_matrix = malloc(sizeof(mi16));
	if(new_matrix == NULL) return NULL;
	new_matrix->m = M->m;
	new_matrix->n = M->n;
	new_matrix->data = malloc(new_matrix->m*sizeof(int16_t*));
	if(new_matrix->data == NULL){
		free(new_matrix);
		return NULL;
	}
	for(int i = 0; i < new_matrix->m; i++){
		new_matrix->data[i] = malloc(new_matrix->n*sizeof(int16_t));
		if(new_matrix->data[i] == NULL){
			for(int k = 0; k < i; k++) free(new_matrix->data[k]);
			free(new_matrix->data);
			free(new_matrix);
			return NULL;
		}
		for(int j = 0; j < new_matrix->n; j++){
			new_matrix->data[i][j] = M->data[i][j];
		}
	}
	return new_matrix;
}

// *******************************************************
// ******************* DESTRUCTORS ***********************
// *******************************************************


void delete_matrix(mf32* matrix){
	if(DEBUG_LOC) printf("Deleting matrix\n");
    for(int i = 0; i < matrix->m; i++){
	    if(DEBUG_LOC) printf("Freeing row %d\n",i);
	    free(matrix->data[i]);
    }
    free(matrix->data);
    free(matrix);
}

void delete_matrix_i16(mi16* matrix){
    for(int i = 0; i < matrix->m; i++){
	    free(matrix->data[i]);
    }
    free(matrix->data);
    free(matrix);
}

// *******************************************************
// ******************* PRETTIFIERS ***********************
// *******************************************************

void print_matrix(mf32* matrix){
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	if(DEBUG_LOC) printf("Window size\nWidth : %d, Height : %d\n", w.ws_col, w.ws_row);
	int width = ((w.ws_col >> 3) < matrix->n) ? ((w.ws_col >> 3) - 1) : matrix->n;
	int height= width;
	if(DEBUG_LOC) printf("Printing a %d x %d matrix\n", height, width);
    for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            printf("%6.2f ", matrix->data[i][j]);
        }
		if(width < matrix->n){
			printf("   ... %6.2f\n", matrix->data[i][matrix->n - 1]);
		}else{
			printf("\n");
		}
    }
	if(height < matrix->m){
		for(int i = 0; i < width; i++){
			printf("   ... ");
		}
		printf("   ...   ...\n");
		for(int i=0; i < width; i++){
			printf("%6.2f ", matrix->data[matrix->m - 1][i]);
		}
		if(width < matrix->n) printf("   ... %6.2f\n", matrix->data[matrix->m - 1][matrix->n - 1]);
	}
}

void print_matrix_i16(mi16* matrix){
    struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	if(DEBUG_LOC) printf("Window size\nWidth : %d, Height : %d\n", w.ws_col, w.ws_row);
	int shrink = ((w.ws_col / 10) < matrix->n) ? 1 : 0;
	int width  = shrink ? ((w.ws_col / 10) - 1) : matrix->n;
	int shrinkh= ((width < matrix->m)) ? 1 : 0;
	int height = shrinkh ? width : matrix->m;
	float F_offset = 1<<FIXED_Q;
	if(DEBUG_LOC) printf("Printing a %d x %d matrix\n", height, width);
	for(int i = 0; i < height; i++){
        for(int j = 0; j < width; j++){
            //printf("%d ", matrix->data[i][j]);
            printf("%6.2f ",((float)matrix->data[i][j] / F_offset));
        }
		if(shrink) printf("   ... %6.2f", ((float)matrix->data[i][matrix->n - 1] / F_offset));
        printf("\n");
    }
	if (shrinkh){
		for(int i=0; i < width; i++) printf("   ... ");
		printf("   ...    ...\n");
		for(int i=0; i < width; i++) printf("%6.2f ",((float) matrix->data[matrix->m - 1][i] / F_offset));
		printf("   ... %6.2f\n", ((float)matrix->data[matrix->m - 1][matrix->n - 1] / F_offset));
	}
}
