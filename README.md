Advanced Computer Systems Homework 1 - Matrix Multiplication with SIMD
===

This repository contains a matrix multiplication routine using the x86 SSE and AVX instruction set extensions for more efficient execution. Functionality for using the routines and creating matrices are provided in `matrix.c` and `matrix.h`. A functionality test and benchmark is written in `main.c`. 

Quickstart
---
**prerequisites**: `gcc` and `make`

To test the routines one may simply build using `make` and run the generated executable `matrix`. Running the program will preform a basic test of the matrix functionality, ensuring agreement between naive and optimized implementations. Run as `matrix n` to specify a dimension of a square matrix to before benchmarking on. Alternatively, you can specify a file, 1|2|3 for which tests to run, the number of repetitions to averge, and the sizes you wish to test to get a LaTeX formatted table output to the file. 

* Ex: `./matrix`
	
	Runs a basic test suite.
* Ex: `./matrix 500`
	
	Runs a test for floating point and fixed point matricies of size 500x500.
* Ex: `touch out.txt && ./matrix out.txt 3 5 100 200 300 400 500`

	Runs tests for matricies of size 100, 200, 300, 400, and 500. Each test is repeated 5 times and averaged for the output. Both floating and fixed tests are run (3 = both tests) outputs test results into out.txt (touch ensures that the file exists before running).

Description
---
Two matrices types are provided:
  
  * `mf32` for single precision floating points
  * `mi16` for 16 bit fixed point 
  
each type is a struct stores the matrix data and dimension information. The matrix data is stored in row major form. The number of fixed point fractional bits is set with the `FIXED_Q` define present in `matrix.h`. Take care when using the fixed point representation and large matrices as overflows are likely due to the accumulating nature of matrix multiplication. 

All functions described are for the `mf32` matrices unless `_i16` is present in the function name. For creating a matrix of random data the function `random_matrix(int n, int m)` is provided, where m and n are the dimensions. Similarity a `zero_matrix(int n, int m)` function is provided.

`mat_mult_naive(mf32* A, mf32* B, mf32* D)` and `mat_mult(mf32* A, mf32* B, mf32* D)` compute the matrix produce A*B=D. Both functions only work on square matrices, and the dimensions or A, B, and D must be the same. The naive function is very slow and optimized and used for comparison. `mat_mult` has both cache optimization and SIMD instructions. If the compiler flag `__AVX__` is set, the implementation of `mat_mult` will use AVX instructions otherwise it will fall back on SSE counterparts. 

A destructor `delete_matrix(mf32* matrix)` is also provided to clean up the dynamically allocated memory of the matrix creation functions. 

An intelligent `print_matrix(mf32* matrix)` prints a matrix to stdout, truncating the result as to not overflow the terminal emulator window. 

`dup_matrix(mf32* M)` creates a new matrix with the same data as M and returns a pointer to the new matrix. 

`transpose(mf32* M)` transposes M in place. 
